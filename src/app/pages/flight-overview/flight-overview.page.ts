import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import { OperationalFlight } from 'src/app/core/api/klm.api.interfaces';
import { FlightsService } from 'src/app/core/data/flights.service';

@Component({
  selector: 'app-flight-overview',
  templateUrl: 'flight-overview.page.html',
  styleUrls: ['flight-overview.page.scss'],
})
export class FlightOverviewPage implements OnInit, OnDestroy {

  flights$: Observable<OperationalFlight[]> = this.flightDataService.flightStatuses$;

  public destroyed = new Subject<any>();

  constructor(
    private flightDataService: FlightsService,
    private router: Router
  ) {}

  ngOnInit() {
    const { App } = Plugins;
    const url = this.router.url;

    // This will update the data when the user navigates to this page
    this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd),
      filter((event: RouterEvent) => event.url === url || event.url === '/'),
      switchMap(_ => this.refreshData()),
      takeUntil(this.destroyed)
    ).subscribe(_ => console.log('Updated data on navigation event'));

    // This will update the data when state is resumed from background on native devices
    App.addListener('appStateChange', (state) => {
      if (state.isActive && url === this.router.url) {
        this.refreshData().pipe(
          takeUntil(this.destroyed)
        ).subscribe(_ => console.log('updated data on resumed state'));
      }
    });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  refreshData(refresh: boolean = false): Observable<any> {
    return this.flightDataService.updateData(refresh);
  }

  doRefresh(event: any) {
    this.refreshData(true).subscribe(
      _ => event.target.complete()
    );
  }

  async loadMoreFlights(event: any) {
    await this.flightDataService.loadMoreFlights();

    event.target.complete();
  }

}
