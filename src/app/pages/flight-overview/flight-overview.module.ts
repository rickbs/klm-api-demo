import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ListItemFlightStatusModule } from 'src/app/components/list-item-flight-status/list-item-flight-status.module';
import { FlightOverviewPageRoutingModule } from './flight-overview-routing.module';
import { FlightOverviewPage } from './flight-overview.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlightOverviewPageRoutingModule,
    ListItemFlightStatusModule
  ],
  declarations: [FlightOverviewPage]
})
export class FlightOverviewPageModule {}
