import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlightOverviewPage } from './flight-overview.page';


const routes: Routes = [
  {
    path: '',
    component: FlightOverviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlightOverviewPageRoutingModule {}
