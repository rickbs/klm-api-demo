import { Injectable } from '@angular/core';
import * as dayjs from 'dayjs';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, take, tap } from 'rxjs/operators';
import { OperationalFlight } from '../api/klm.api.interfaces';
import { KLMFlightsParams } from '../api/klm.params.interfaces';
import { KlmService } from '../api/klm.service';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  private _flightStatuses = new BehaviorSubject<OperationalFlight[]>([]);
  private _pageNumber = 1;

  public readonly flightStatuses$: Observable<OperationalFlight[]> = this._flightStatuses.asObservable().pipe(
    filter(res => res !== []),
    distinctUntilChanged()
  );

  constructor(private klmApiService: KlmService) { }

  updateData(refresh = false): Observable<OperationalFlight[]> {
    if(refresh) {
      this._pageNumber = 1;
    }

    return this.retrieveDataFromApi().pipe(
      tap(data => console.log('flightstatus objects updated', data)),
      tap(res => this._flightStatuses.next(res))
    );
  }

  loadMoreFlights(): Promise<OperationalFlight[]> {
    this._pageNumber++;

    return new Promise((resolve, reject) => {
      combineLatest([
        this.retrieveDataFromApi(),
        this.flightStatuses$
      ]).pipe(
        take(1),
        map(res => [...res[1], ...res[0]]),
        tap(res => this._flightStatuses.next(res))
      ).subscribe(
        data => {
          console.log('flightstatus objects updated on scroll', data);
          resolve(data);
        },
        error => {
          reject(error);
        }
      );
    });
  }

  private retrieveDataFromApi(): Observable<OperationalFlight[]> {
    const params: KLMFlightsParams = {
      startRange: dayjs().utc().format(),
      endRange: dayjs().add(1, 'd').utc().format(),
      timeType: 'U',
      movementType: 'D',
      timeOriginType: 'S',
      arrivalCity: 'AMS',
      pageSize: 10,
      pageNumber: this._pageNumber
    };
    return this.klmApiService.getFlights(params).pipe(
      tap(res => this._pageNumber = res.page.pageNumber),
      map(res => res.operationalFlights)
    );
  }
}
