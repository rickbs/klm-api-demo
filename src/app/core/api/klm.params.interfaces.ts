export interface KLMFlightsParams {
    startRange: string;
    endRange: string;
    movementType?: 'A'|'D';
    timeOriginType?: 'S'|'M'|'I'|'P';
    timeType?: 'U'|'L';
    origin?: string;
    departureCity?: string;
    destination?: string;
    arrivalCity?: string;
    carrierCode?: string;
    flightNumber?: number;
    aircraftRegistration?: string;
    aircraftType?: string;
    pageNumber?: number;
    pageSize?: number;
  }
