import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FlightStatus } from './klm.api.interfaces';
import { KLMFlightsParams } from './klm.params.interfaces';

const HTTP_METHODS = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete',
};

@Injectable({
  providedIn: 'root'
})
export class KlmService {

  public defaultHeaders = new HttpHeaders({
    Accept: 'application/hal+json;version=com.afkl.operationalflight.v3',
    'Accept-Language': 'en-US',
  });

  baseUrl = environment.klm.endpoint;

  constructor(
    private http: HttpClient
  ) { }

  getFlights(params: KLMFlightsParams): Observable<FlightStatus> {
    return this.request(HTTP_METHODS.GET, 'flightstatus', null, params);
  }

  private request(
    method: string,
    url: string,
    body: any = {},
    params: any = {}
  ): Observable<any> {
    let headers = this.defaultHeaders;

    if (environment && environment.klm.apiKey) {
      headers = headers.append('api-key', `${environment.klm.apiKey}`);
    }

    const options = {
      headers,
      params: new HttpParams({ fromObject: params }),
      body: new HttpParams({ fromObject: body }),
    };

    return this.http.request(method, `${this.baseUrl}${url}`, options);
  }
}
