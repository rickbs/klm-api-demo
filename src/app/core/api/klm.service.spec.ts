import { TestBed } from '@angular/core/testing';

import { KlmService } from './klm.service';

describe('KlmService', () => {
  let service: KlmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KlmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
