export interface Airline {
    code: string;
    name: string;
}

export interface Airline2 {
    code: string;
    name: string;
}

export interface CodeShareRelation {
    marketingFlightNumber: number;
    airline: Airline2;
    code: string;
    type: string;
    flightScheduleDate: string;
}

export interface Country {
    areaCode: string;
    code: string;
    name: string;
    nameLangTranl: string;
    euroCountry: string;
    euCountry: string;
}

export interface City {
    code: string;
    name: string;
    nameLangTranl: string;
    country: Country;
}

export interface Location {
    latitude: number;
    longitude: number;
}

export interface Places {
    terminalCode: string;
    aerogareCode: string;
    boardingTerminal: string;
    checkInZone: string[];
    checkInAerogare: string;
    gateNumber: string[];
    parkingPosition: string;
    boardingPier: string;
    boardingContactType: string;
    parkingPositionType: string;
    departureTerminal: string;
    parkingPositionCustomStatus: string;
    pierCode: string;
}

export interface Airport {
    code: string;
    name: string;
    nameLangTranl: string;
    city: City;
    location: Location;
    places: Places;
}

export interface Times {
    scheduled: Date;
    latestPublished: Date;
    actual?: Date;
    actualTakeOffTime?: Date;
    estimatedPublic?: Date;
}

export interface BoardingTimes {
    plannedBoardingTime: Date;
    gateCloseTime: Date;
}

export interface DepartureInformation {
    airport: Airport;
    times: Times;
    boardingTimes: BoardingTimes;
}

export interface Country2 {
    areaCode: string;
    code: string;
    name: string;
    nameLangTranl: string;
    euroCountry: string;
    euCountry: string;
}

export interface City2 {
    code: string;
    name: string;
    nameLangTranl: string;
    country: Country2;
}

export interface Location2 {
    latitude: number;
    longitude: number;
}

export interface Places2 {
    arrivalPositionTerminal: string;
    gateNumber: string[];
    baggageBelt: string[];
    aerogareCode: string;
    terminalCode: string;
    disembarkingAerogare: string;
    parkingPosition: string;
    pierCode: string;
    parkingPositionType: string;
    disembarkingContactType: string;
    arrivalTerminal: string;
    parkingPositionCustomStatus: string;
    arrivalHall: string[];
    firstBagOnBeltTime?: Date;
    lastBagOnBeltTime?: Date;
}

export interface Airport2 {
    code: string;
    name: string;
    nameLangTranl: string;
    city: City2;
    location: Location2;
    places: Places2;
}

export interface Estimated {
    value: Date;
}

export interface Times2 {
    scheduled: Date;
    latestPublished: Date;
    estimated: Estimated;
    actual?: Date;
    actualTouchDownTime?: Date;
    estimatedArrival?: Date;
    estimatedTouchDownTime?: Date;
}

export interface ArrivalInformation {
    airport: Airport2;
    times: Times2;
}

export interface Aircraft {
    typeCode: string;
    typeName: string;
    ownerAirlineCode: string;
    ownerAirlineName: string;
    physicalPaxConfiguration: string;
    physicalFreightConfiguration: string;
    operationalConfiguration: string;
    cockpitCrewEmployer: string;
    cabinCrewEmployer: string;
    registration: string;
    subFleetCodeId: string;
    wifiEnabled: string;
    satelliteConnectivityOnBoard: string;
}

export interface DelayInformation {
    delayReasonCodePublic: string;
    delayReasonPublicShort: string;
    delayReasonPublicLong: string;
}

export interface Irregularity {
    cancelled: string;
    delayDurationPublic: string;
    delayDurationArrival: string;
    delayReasonCodePublic: string[];
    delayReasonPublicLangTransl: string[];
    delayInformation: DelayInformation[];
}

export interface OtherFlightLegStatuses {
    boardingStatus: string;
}

export interface FlightLeg {
    status: string;
    statusName: string;
    publishedStatus: string;
    departureInformation: DepartureInformation;
    arrivalInformation: ArrivalInformation;
    legStatusPublic: string;
    legStatusPublicLangTransl: string;
    passengerCustomsStatus: string;
    serviceType: string;
    serviceTypeName: string;
    scheduledFlightDuration: string;
    completionPercentage: string;
    timeZoneDifference: string;
    aircraft: Aircraft;
    irregularity: Irregularity;
    internalLegStatusArrFocus: boolean;
    departureDateTimeDifference: string;
    arrivalDateTimeDifference: string;
    timeToArrival: string;
    otherFlightLegStatuses: OtherFlightLegStatuses;
}

export interface EquivalentFlightData {
    id: string;
    flightScheduleDate: string;
    airlineCode: string;
    flightNumber: string;
}

export interface PreviousFlightData {
    id: string;
    flightScheduleDate: string;
    airlineCode: string;
    flightNumber: string;
}

export interface OnwardFlightData {
    id: string;
    flightScheduleDate: string;
    airlineCode: string;
    flightNumber: string;
}

export interface FlightRelations {
    equivalentFlightData: EquivalentFlightData;
    previousFlightData: PreviousFlightData;
    onwardFlightData: OnwardFlightData;
}

export interface OperationalFlight {
    flightNumber: number;
    flightScheduleDate: string;
    id: string;
    route: string[];
    airline: Airline;
    codeShareRelations: CodeShareRelation[];
    flightStatusPublic: string;
    flightStatusPublicLangTransl: string;
    flightLegs: FlightLeg[];
    internalStatusArrFocus: boolean;
    haul: string;
    flightRelations: FlightRelations;
}

export interface Page {
    pageSize: number;
    pageNumber: number;
    fullCount: number;
    pageCount: number;
    totalPages: number;
}

export interface FlightStatus {
    operationalFlights: OperationalFlight[];
    page: Page;
}
