import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ListItemFlightStatusComponent } from './list-item-flight-status.component';

@NgModule({
  declarations: [ListItemFlightStatusComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ListItemFlightStatusComponent]
})
export class ListItemFlightStatusModule { }
