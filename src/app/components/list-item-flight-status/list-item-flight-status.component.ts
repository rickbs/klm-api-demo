import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import * as dayjs from 'dayjs';
import { interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { OperationalFlight, Times2 } from 'src/app/core/api/klm.api.interfaces';

@Component({
  selector: 'app-list-item-flight-status',
  templateUrl: './list-item-flight-status.component.html',
  styleUrls: ['./list-item-flight-status.component.scss'],
})
export class ListItemFlightStatusComponent implements OnInit, OnDestroy {

  @Input() flight: OperationalFlight | undefined;

  public destroyed = new Subject<any>();

  timeLeft = '';

  constructor() {}

  ngOnInit() {
    const source = interval(1000);
    const times = this.flight?.flightLegs[0].arrivalInformation.times;

    if(times) {
      this.showArrivalTime(times);
      source
        .pipe(takeUntil(this.destroyed))
        .subscribe(_ => this.showArrivalTime(times));
    }
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  shouldShowArrivalInformation(status: string) {
    if(status === 'ON_TIME' || status === 'IN_FLIGHT' || status === 'NEW_DEPARTURE_TIME') {
      return true;
    } else {
      return false;
    }
  }

  getFlightStatusColor(status: string) {
    if(status === 'ON_TIME') {
      return 'success';
    } else if(status === 'DEPARTED') {
      return 'primary';
    } else if(status === 'IN_FLIGHT' || status === 'NEW_DEPARTURE_TIME') {
      return 'secondary';
    } else if (status === 'CANCELLED') {
      return 'danger';
    } else {
      return 'danger';
    }
  }

  showArrivalTime(times: Times2) {
    const estimated = dayjs(times.estimated.value);
    const currentTime = dayjs();

    const diffBetweenToday = dayjs(estimated).diff(currentTime);
    this.timeLeft = this.formatMsToReadableTime(diffBetweenToday);
  }

  private formatMsToReadableTime(milliseconds: number): string {
    let seconds = Math.round((milliseconds / 1000));
    let minutes = Math.round((seconds / 60));
    let hours = Math.round((minutes / 60));

    seconds %= 60;
    minutes %= 60;
    hours %= 24;

    let secondsString = seconds.toString();
    let minutesString = minutes.toString();
    let hoursString = hours.toString();

    if (seconds < 10) {
      secondsString = `0${seconds}`;
    }
    if (minutes < 10) {
      minutesString = `0${minutes}`;
    }
    if (hours < 10) {
      hoursString = `0${hours}`;
    }

    return `${hoursString}:${minutesString}:${secondsString}`;
  }

}
