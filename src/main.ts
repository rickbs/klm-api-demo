import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as dayjs from 'dayjs';
import * as utc from 'dayjs/plugin/utc';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';


if (environment.production) {
  enableProdMode();

  if (window) {
    window.console.log = () => {};
  }
}

// Add utc plugin for dayjs
dayjs.extend(utc);

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
