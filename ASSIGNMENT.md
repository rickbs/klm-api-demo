# Assignment

- Using the flight status API, create a page that shows the status of the flights that are scheduled from now until tomorrow.
- The status of the flight should be easily recognizable (on time, delayed, cancelled, etc.).
- Add functionalities to make your application user friendly (for example: a search on flight number, pagination, etc. etc.).